#!/usr/bin/env python
# -*- coding: utf-8 -*-

from optparse import OptionParser
from scapy.all import srp, Ether, ARP, get_if_addr

def main():
    if_list = ['en0', 'en1', 'fw0', 'eth0', 'eth1', 'eth2', 'eth3', 'wlan0', 'wlan1']
    for inetf in if_list:
        try:
            my_ip_addr = get_if_addr(inetf)
        except KeyError:
            pass
        except TypeError:
            pass
    usage = "%prog [options]"
    parser = OptionParser(usage)
    parser.add_option('-r', '--reseau',
        dest="network",
        help="specifie le reseau a scanner, sous la forme CIDR [defaut : %default]",
        type="string",
        default=my_ip_addr+"/24",
        )
    parser.add_option('-e', '--exclure',
        dest="non_necessaires",
        help="ajoute une adresse IP a exclure du traitement",
        type="string",
        action="append",
        default=[my_ip_addr],
        )
    
    (options, args) = parser.parse_args()

    answered,unanswered = srp(Ether(dst="ff:ff:ff:ff:ff:ff")/ARP(pdst=options.network),timeout=4)
    for sent,received in answered:
        if (options.non_necessaires is not None) and (received.getlayer(ARP).psrc not in options.non_necessaires):
            print(received.getlayer(Ether).src + " => " + received.getlayer(ARP).psrc)


if __name__ == "__main__":
    main()
