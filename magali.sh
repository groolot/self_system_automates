#!/bin/sh -x

if [ -f $1 ]; then
    FILE=${1}
    DATE=$(egrep "^Date:.*$" ${FILE} | awk '{day=$3} $4=="Jan"{month="01"} $4=="Feb"{month="02"} $4=="Mar"{month="03"} $4=="Apr"{month="04"} $4=="May"{month="05"} $4=="Jun"{month="06"}$4=="Jul"{month="07"}$4=="Aug"{month="08"}$4=="Sep"{month="09"}$4=="Oct"{month="10"}$4=="Nov"{month="11"}$4=="Dec"{month="12"} {year=$5}{printf "%04i%02i%02i",year,month,day}')
    YEAR=$(egrep "^Date:.*$" ${FILE} | awk '{print $5}' | sort -u)
    HOUR=$(egrep "^Date:.*$" ${FILE} | egrep -o "[0-9]{2}:[0-9]{2}:[0-9]{2}" | awk 'BEGIN{FS=":"}{printf "%02ih%02im%02is",$1,$2,$3}')

    FROM=$(egrep "^Reply-To:.*$" ${FILE} | grep -o "magali">/dev/null; echo $?)
    if [ ${FROM} -eq 0 ]; then
	FROM="magali"
	TO="gregory"
    else
	FROM="gregory"
	TO="magali"
    fi

    mkdir -p ./new/${YEAR}
    if [ $? -eq 1 ]; then
	echo "ERROR: can not create the directory => ./new/${YEAR}"
    fi
    
    cat ${FILE} > ./new/${YEAR}/${DATE}.${HOUR}.${FROM}.vers.${TO}.eml

    if [ $? -eq 1 ]; then
	echo "ERROR: ${FILE}, Date: ${DATE}, From: ${FROM}, TO: ${TO}"
    fi
fi
