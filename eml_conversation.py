#!/usr/bin/env python

import os
import sys
import re
import argparse
import email
import datetime

COMMASPACE = ', '


def main():
    parser = argparse.ArgumentParser(description="Read the contents of a directory and create a LaTeX conversation.", epilog="Author: Gregory DAVID <groolot@groolot.net", prog="eml2conversation.py")
    parser.add_argument('-d', '--directory', required=True, dest='directory', metavar='EML_DIR', help="Directory containing the conversation as multiple email message .eml")
    parser.add_argument('-o', '--output', required=True, dest='output', metavar='LATEX_FILE', help="Output LaTeX file for the conversation")
    args = parser.parse_args()

    outputfile = open(args.output,'w')

    for filename in os.listdir(args.directory):
        path = os.path.join(args.directory, filename)
        if not os.path.isfile(path):
            continue
        else:
            fp = open(path, 'rb')
            pattern = re.compile("[0-9]{8}\.[0-9]{2}h[0-9]{2}m[0-9]{2}s")
            date = datetime.datetime.strptime(pattern.search(filename).group(0), "%Y%m%d.%Hh%Mm%Ss")
            eml = email.message_from_file(fp)
            fp.close()
            if eml.is_multipart():
                payload = eml.get_payload()[0].get_payload(decode=True)
            else:
                payload = eml.get_payload(decode=True)
            if payload is None:
                payload = "Pas de message"
            if eml['from'] is not None:
                emetteur = eml['from']
            else:
                emetteur = "Inconnu"
            if eml['to'] is not None:
                destinataire = eml['to']
            else:
                destinataire = "Inconnu"
            payload = re.sub(r'Une messagerie gratuite.*',r'',payload, flags=re.DOTALL)
            #payload = re.sub(r'>.*',r'',payload, flags=re.DOTALL)
            content = "\email{"+emetteur+"}{"+destinataire+"}{"+str(date)+"}{"+payload+"}\n\r"
            outputfile.write(content)

    outputfile.close()

if __name__ == '__main__':
    main()
