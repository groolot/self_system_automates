#!/bin/bash
cd ~/ArchivesMails
/Applications/TrueCrypt.app/Contents/MacOS/TrueCrypt --mount ~/Documents/archives_mails.tc
egrep -l -r "samiemerly" ./ | sort -u | zip -b /tmp /Volumes/ArchivesMails/samantha.zip -@ | awk 'BEGIN{FS=":";addings=0;updates=0} $1=="updating" {updates+=1} $1=="  adding" {addings+=1} END{print "samantha.zip\n\tupdates : ", updates, "\n\taddings : ", addings}'
egrep -l -r "magali\.ouisse" ./ | sort -u | zip -b /tmp /Volumes/ArchivesMails/magali.zip -@ | awk 'BEGIN{FS=":";addings=0;updates=0} $1=="updating" {updates+=1} $1=="  adding" {addings+=1} END{print "magali.zip\n\tupdates : ", updates, "\n\taddings : ", addings}'
egrep -L -r "(magali\.ouisse|samiemerly)" ./ | sort -u | zip -b /tmp /Volumes/ArchivesMails/archives.zip -@ | awk 'BEGIN{FS=":";addings=0;updates=0} $1=="updating" {updates+=1} $1=="  adding" {addings+=1} END{print "archives.zip\n\tupdates : ", updates, "\n\taddings : ", addings}'
cd ~/Downloads/depuis_mails
find . -name "De*.mp3" | zip -b /tmp /Volumes/ArchivesMails/vocaux.zip -@ | awk 'BEGIN{FS=":";addings=0;updates=0} $1=="updating" {updates+=1} $1=="  adding" {addings+=1} END{print "vocaux.zip\n\tupdates : ", updates, "\n\taddings : ", addings}'
/Applications/TrueCrypt.app/Contents/MacOS/TrueCrypt --dismount ~/Documents/archives_mails.tc
