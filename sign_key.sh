#!/bin/bash
for KEY_ID in $* ;
do
	gpg --keyserver pgp.mit.edu --recv-keys ${KEY_ID}
	gpg --fingerprint ${KEY_ID}
	gpg --sign-key --ask-cert-level ${KEY_ID}
	gpg --keyserver pgp.mit.edu  --send-key ${KEY_ID}
done
