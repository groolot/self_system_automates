#!/bin/bash

EXECDIR=/Users/groolot/bin/net_install_debian
DHCPD_BIN=/opt/local/sbin/dhcpd
DHCPD_OPT="-cf ${EXECDIR}/dhcpd.conf -lf ${EXECDIR}/dhcpd.lease -pf ${EXECDIR}/dhcpd.pid en0"
TFTPD_BIN=/opt/local/sbin/tftpd
TFTPD_OPT="-l -s ${EXECDIR}"

test -x $DHCPD_BIN || exit 0
test -x $TFTPD_BIN || exit 0

set -e

function start_them
{
	echo -n "Starting dhcpd: "
	sudo ${DHCPD_BIN} ${DHCPD_OPT} && echo "started."
	echo -n "Starting tftpd: "
	sudo ${TFTPD_BIN} ${TFTPD_OPT} && echo "started."
}
function stop_them
{
	echo -n "Stopping dhcpd: "
	sudo kill $(cat ${EXECDIR}/dhcpd.pid) && echo "stopped."
	echo -n "Stopping tftpd: "
	sudo killall $(basename ${TFTPD_BIN}) && echo "stopped."
}

case "$1" in
  start)
    start_them
	;;
  stop)
    stop_them
	;;
  restart)
	echo "Restarting everything:"
	stop_them
	start_them
	;;
  *)
	echo "Usage: $0 {start|stop|restart}" >&2
	exit 1
	;;
esac

exit 0
